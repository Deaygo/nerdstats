package nu.nerd.nerdstats;

import org.bukkit.plugin.java.JavaPlugin;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.Tuple;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

public class NerdStats extends JavaPlugin {

    private JedisPool jedisPool;
    private String serverName;

    public void onEnable() {
        loadConfig();
    }

    /**
     * Save the default config if not present and load values from the config file.
     * Redis connection details are handled by establishRedisConnection()
     */
    public void loadConfig() {
        this.saveDefaultConfig();
        this.serverName = this.getConfig().getString("server_name", null);
    }

    /*
    * Reload the configuration file and redis
    */
    public void reload() {
        if (jedisPool != null) {
            jedisPool.destroy();
            jedisPool = null;
        }
        this.reloadConfig();
        this.serverName = this.getConfig().getString("server_name", null);
        establishRedisConnection();
    }

    public void onDisable() {
        if (this.jedisPool != null) {
            this.jedisPool.destroy();
        }
    }

    public void incrementScore(String key, String member, double amount) throws Exception {
        Jedis j = getJedisResource();
        j.zincrby(key, amount, member);
    }

    public Double getScore(String key, String member) throws Exception {
        Jedis j = getJedisResource();
        return j.zscore(key, member);
    }

    public Map<String, Double> getTopXScores(String key, int x) throws Exception {
        HashMap<String, Double> scores = new HashMap<>();

        Jedis j = getJedisResource();
        Set<Tuple> ret = j.zrevrangeWithScores(key, 0, x);

        for (Tuple t : ret) {
            scores.put(t.getElement(), t.getScore());
        }

        return scores;
    }

    /**
     * Check out a Jedis resource from the pool.
     * Don't forget to return it to the pool after...
     * @throws Exception
     */
    private Jedis getJedisResource() throws Exception {
        if (jedisPool != null) {
            return jedisPool.getResource();
        } else {
            throw new Exception("NerdStats cannot connect to Redis.");
        }
    }

    /**
     * Connect to the redis server
     */
    private void establishRedisConnection() {

        Boolean enabled = getConfig().getBoolean("redis.enabled", false);
        String server = getConfig().getString("redis.server", "localhost");
        String password = getConfig().getString("redis.password", null);
        Integer port = getConfig().getInt("redis.port", 6379);
        Integer timeout = getConfig().getInt("redis.timeout", 30);
        Integer connections = getConfig().getInt("redis.max_connections", 4);

        JedisPoolConfig poolconfig = new JedisPoolConfig();
        if (connections < 2) connections = 2; // redis requires at least two connections for pubsub
        poolconfig.setMaxTotal(connections);

        if (enabled && server != null && password != null && !password.equals("")) {
            jedisPool = new JedisPool(poolconfig, server, port, timeout, password);
        } else if (enabled && server != null) {
            jedisPool = new JedisPool(poolconfig, server, port, timeout);
        } else {
            getLogger().log(Level.WARNING, "Redis is not configured. Statistics tracking will not be available.");
            return;
        }
    }
}
